# **maker** project

The package provides a tool for composing a scene and editing
properties of objects in the scene. The tool depends on the
**age libraries**.

Include this repository as a submodule to your project's repository.
