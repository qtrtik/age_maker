#ifndef MAKER_MODULE_HPP_INCLUDED
#   define MAKER_MODULE_HPP_INCLUDED

namespace com { struct Folder; }

namespace mak {

void boot(com::Folder* ctx_root);
void shutdown(com::Folder* ctx_root);

}

#endif
