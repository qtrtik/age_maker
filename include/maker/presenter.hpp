#ifndef MAKER_PRESENTER_HPP_INCLUDED
#   define MAKER_PRESENTER_HPP_INCLUDED

#   include <com/runner.hpp>
#   include <math/math.hpp>

namespace com { struct Frame; }

namespace mak {

struct Presenter : public com::Runner
{
    static inline std::string self_name() { return "presenter.run"; }

    Presenter();
    ~Presenter() override;

    void next_round() override;

protected:

    void initialize() override;
    void release() override;
};

}

#endif
