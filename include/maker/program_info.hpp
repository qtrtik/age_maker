#ifndef MAKER_PROGRAM_INFO_HPP_INCLUDED
#   define MAKER_PROGRAM_INFO_HPP_INCLUDED

#   include <string>

namespace mak {

std::string  get_program_name();
std::string  get_program_version();
std::string  get_program_description();

}

#endif
