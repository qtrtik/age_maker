#ifndef MAKER_UPDATER_HPP_INCLUDED
#   define MAKER_UPDATER_HPP_INCLUDED

#   include <com/runner.hpp>
#   include <math/math.hpp>

namespace com { struct Frame; }

namespace mak {

struct Updater : public com::Runner
{
    static inline std::string self_name() { return "updater.run"; }

    Updater();
    ~Updater() override;

    void next_round() override;

protected:

    void initialize() override;
    void release() override;
};

}

#endif
