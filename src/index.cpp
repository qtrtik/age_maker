#include <maker/index.hpp>

namespace mak {

Index const& Index::instance()
{
    static Index const idx;
    return idx;
}

Index::Index()
    : m_root { com::Folder::root()->find<com::Folder>("maker") }
    , m_camera_frame{ m_root->find<com::Folder>("camera")->find<com::Frame>(com::Frame::self_file_name()) }
    , m_grid_frame{ m_root->find<com::Folder>("grid")->find<com::Frame>(com::Frame::self_file_name()) }
{}

}
