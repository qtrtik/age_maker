#include <maker/program_info.hpp>
#include <maker/program_options.hpp>
#include <maker/module.hpp>
#include <osi/run.hpp>
#include <utils/config.hpp>
#include <utils/timeprof.hpp>
#include <utils/log.hpp>
#include <filesystem>
#include <fstream>
#include <memory>
#include <stdexcept>
#include <iostream>
#if PLATFORM() == PLATFORM_WINDOWS()
#   pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup") 
#endif

#if BUILD_RELEASE() == 1
static void save_crash_report(std::string const& crash_message)
{
    std::cout << "ERROR: " << crash_message << "\n";
    std::ofstream  ofile( mak::get_program_name() + "_CRASH.txt", std::ios_base::app );
    ofile << crash_message << "\n";
}
#endif

int main(int argc, char* argv[])
{
#if BUILD_RELEASE() == 1
    try
#endif
    {
        LOG_INITIALISE(mak::get_program_name(), LSL_WARNING);
        mak::initialise_program_options(argc,argv);
        if (mak::get_program_options()->helpMode())
            std::cout << mak::get_program_options();
        else if (mak::get_program_options()->versionMode())
            std::cout << mak::get_program_version() << "\n";
        else
        {
            osi::run({
                .window_title = "maker",
                .resizable = true,
                .windowed = true,
                .user_modules = {
                    { mak::boot, mak::shutdown },
                    }
                });
            TMPROF_PRINT_TO_FILE(mak::get_program_name(),true);
        }
    }
#if BUILD_RELEASE() == 1
    catch(std::exception const& e)
    {
        try { save_crash_report(e.what()); } catch (...) {}
        return -1;
    }
    catch(...)
    {
        try { save_crash_report("Unknown exception was thrown."); } catch (...) {}
        return -2;
    }
#endif
    return 0;
}
