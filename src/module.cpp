#include <maker/module.hpp>
#include <maker/updater.hpp>
#include <maker/presenter.hpp>
#include <maker/index.hpp>
#include <com/frame.hpp>

namespace mak {

void boot(com::Folder* const ctx_root)
{
    com::Folder* const root_mak = ctx_root->push_back<com::Folder>("maker");
    root_mak->push_back<com::Folder>("camera")->push_back<com::Frame>();
    root_mak->push_back<com::Folder>("grid")->push_back<com::Frame>();
    root_mak->push_back<Updater>(); 
    root_mak->push_back<Presenter>(); 
    mak::index();
}

void shutdown(com::Folder* const ctx_root)
{
    com::Folder* const root_mak = ctx_root->find<com::Folder>("maker");
    root_mak->erase(root_mak->find<Presenter>(Presenter::self_name()));
    root_mak->erase(root_mak->find<Updater>(Updater::self_name()));
    root_mak->erase(root_mak->find<com::Folder>("grid"));
    root_mak->erase(root_mak->find<com::Folder>("camera"));
    ctx_root->erase(root_mak);
}

}
