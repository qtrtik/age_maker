#include <maker/presenter.hpp>
#include <maker/index.hpp>
#include <gfx/index.hpp>
#include <osi/index.hpp>
#include <com/math_files.hpp>

namespace mak {

Presenter::Presenter()
    : com::Runner{ self_name() }
{}

Presenter::~Presenter()
{}

void Presenter::initialize()
{
    osi::presenters()->push_back<com::Folder>("maker")->push_back<com::Link>(self_name() + ".link", this);

    gfx::object_system()->push_frame_back(
        gfx::object_system()->insert_object(
            { "grid" },
            gfx::material_system()->insert_material("grid_material", gfx::shader_system()->forward_unlit_vertex_color_shader(), { "maker" }),
            gfx::buffer_generators()->insert_procedural_grid()
            ),
        grid_frame()
        );

    // --- BEGIN test_box ---

    com::Folder* const test_box = gfx::object_system()->insert_object(
            { "maker", "test_box" },
            gfx::material_system()->insert_default_material("box_material", { "maker" }, vec3{ 0.75f, 0.75f, 0 } ),
            gfx::buffer_generators()->insert_procedural_box_solid({ 0.5f, 0.5f, 0.5f } , "test_box", { "maker" })
            );
    gfx::object_system()->push_frame_back(
        test_box ,
        root()->push_back<com::Folder>("test_box")->push_back<com::Frame>()
        );
    gfx::object_system()->get_uniforms(test_box)->push_back<com::FileVec3>("material_ambient_color", vec3{ 0.75f, 0.75f, 0 });

    // The second instance
    com::Frame* const frame2 = root()->push_back<com::Folder>("test_box_2")->push_back<com::Frame>();
    frame2->move_origin({ 0.5f, 0, 1.5f });
    gfx::object_system()->push_frame_back(test_box, frame2);

    // --- END test_box ---

    // --- BEGIN directional light ---

    com::Frame* const frame_light = root()->push_back<com::Folder>("directional_light")->push_back<com::Frame>();
    com::Folder* const dir_light = gfx::light_system()->insert_light<gfx::DirectionalLight>(
            { "global", "directional" },
            frame_light,
            vec4{ 0.75f, 0.75f, 0.75f, 1 }
            );

    // --- END directional light ---

    gfx::object_system()->insert_light(test_box, dir_light);
    gfx::light_system()->insert_shadow_caster(dir_light, test_box);
}

void Presenter::release()
{
    osi::presenters()->find<com::Folder>("maker")->erase(self_name() + ".link");

    root()->erase("test_box");
}

void Presenter::next_round()
{
    gfx::renderer()->clear_render_buffers();
    gfx::renderer()->present_collection(gfx::object_system()->objects(), gfx::Renderer::pipeline::FORWARD);
}

}
