#include <maker/program_info.hpp>

namespace mak {

std::string  get_program_name()
{
    return "maker";
}

std::string  get_program_version()
{
    return "0.1";
}

std::string  get_program_description()
{
    return "TODO: Describe here the purpose of the tool, what functionality\n"
           "it provides, and briefly explain basic idea(s)/principle(s) of its\n"
           "functionality. (Note: end each line by '\\n'.)"
           ;
}

}
