#include <maker/updater.hpp>
#include <maker/index.hpp>
#include <gfx/index.hpp>
#include <osi/index.hpp>
#include <com/frame.hpp>

namespace mak {

Updater::Updater()
    : com::Runner{ self_name() }
{}

Updater::~Updater()
{}

void Updater::initialize()
{
    osi::updaters()->push_back<com::Folder>("maker")->push_back<com::Link>(self_name() + ".link", this);

    camera_frame()->set_frame({
        { 10, 10, 4 },
        { 0.293152988f, 0.245984003f, 0.593858004f, 0.707732975f }
        });
    gfx::camera_system()->set_frame(camera_frame());
}

void Updater::release()
{
    osi::updaters()->find<com::Folder>("maker")->erase(self_name() + ".link");
}

void Updater::next_round()
{
    if (osi::mouse()->down().contains("MouseRight"))
    {
        scalar const linear_speed = 5.0f;
        scalar const rotation_speed_mult = 5.0f;
        vec2 const mouse_angle =
            (rotation_speed_mult / gfx::camera_system()->default_camera()->near()) *
            (vec2{ osi::mouse()->pos_delta() } * osi::window()->pixel_size_in_meters());

        basis3 const camera_basis{ to_mat3x3(camera_frame()->frame().rotation()) };
        if (osi::keyboard()->down().contains("W") || osi::keyboard()->down().contains("Up"))
            camera_frame()->move_origin((-linear_speed * osi::timer()->dt()) * camera_basis.axis<2>());
        else if (osi::keyboard()->down().contains("S") || osi::keyboard()->down().contains("Down"))
            camera_frame()->move_origin((linear_speed * osi::timer()->dt()) * camera_basis.axis<2>());

        if (osi::keyboard()->down().contains("A") || osi::keyboard()->down().contains("Left"))
            camera_frame()->move_origin((-linear_speed * osi::timer()->dt()) * camera_basis.axis<0>());
        else if (osi::keyboard()->down().contains("D") || osi::keyboard()->down().contains("Right"))
            camera_frame()->move_origin((linear_speed * osi::timer()->dt()) * camera_basis.axis<0>());

        if (osi::keyboard()->down().contains("Q"))
            camera_frame()->move_origin((-linear_speed * osi::timer()->dt()) * camera_basis.axis<1>());
        else if (osi::keyboard()->down().contains("E"))
            camera_frame()->move_origin((linear_speed * osi::timer()->dt()) * camera_basis.axis<1>());

        camera_frame()->move_rotation(to_quat(-get<1>(mouse_angle), camera_basis.axis<0>()));
        camera_frame()->move_rotation(to_quat(-get<0>(mouse_angle), axis<vec3, 2>()));
    }
}

}
